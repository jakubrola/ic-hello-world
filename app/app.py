from flask import Flask, json
import http.client
import os

api = Flask(__name__)


@api.route('/hello', methods=['GET'])
def get_Hello():
    
    response = {"hello": "Hello from Ideal Cities minikube"}
    return json.dumps(response)

if __name__ == '__main__':
    api.run(debug=False, host="0.0.0.0")
